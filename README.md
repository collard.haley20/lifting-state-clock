To run, `npm start`

1. Begin app with existing clock on page.
2. Allow for application to display more than one timezone
3. Abstract clock to not manage exact location, allow for any locations. Setup a dashboard
4. Prepare dashboard to not control the locations, allow for locations to come from elsewhere